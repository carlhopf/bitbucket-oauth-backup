#!/usr/bin/env node
var request = require('request');
var async = require('async');
var path = require('path');
var cp = require('child_process');
var mkdirp = require('mkdirp');
var rimraf = require('rimraf');

var argv = process.argv;
var CLIENT_ID = argv[2]
var CLIENT_SECRET = argv[3];
var BITBUCKET_USER = argv[4];
var REFRESH_TOKEN = argv[5];

var DIR = path.join(process.cwd(), 'bitbucket-backup-' + BITBUCKET_USER);
mkdirp.sync(DIR);

if (!REFRESH_TOKEN) throw new Error('refresh token missing');

var getAccessToken = function(cb) {
  request({
    method: 'POST',
    uri: 'https://bitbucket.org/site/oauth2/access_token',
    auth: {
      user: CLIENT_ID,
      pass: CLIENT_SECRET,
    },
    form: {
      refresh_token: REFRESH_TOKEN,
      grant_type: 'refresh_token',
    },
    json: true,
  }, function(err, res, body) {
    cb(err, body && body.access_token);
  });
};

var getRepos = function(accessToken, cb) {
  request({
    method: 'GET',
    uri: 'https://bitbucket.org/api/1.0/user/repositories',
    qs: {
      access_token: accessToken,
    },
    json: true,
  }, function(err, res, body) {
    cb(err, body);
  });
};

var cloneRepo = function(accessToken, repo, cb) {
  var uri = 'https://x-token-auth:' + accessToken + '@bitbucket.org/' +
    repo.owner + '/' + repo.name + '.git';

  cp.exec('git clone ' + uri, {
    cwd: DIR,
  }, function(err) {
    console.log('cloned', repo.name);
    cb(err);
  });
};

async.auto({
  accessToken: getAccessToken,
  repos: ['accessToken', (cb, res) => {
    getRepos(res.accessToken, cb);
  }],
  clean: ['repos', (cb, res) => {
    async.eachSeries(res.repos, (repo, cb) => {
      console.log('clean', repo.name);
      rimraf(path.join(DIR, repo.name), cb);
    }, cb);
  }],
  clone: ['clean', 'accessToken', 'repos', (cb, res) => {
    async.eachSeries(
      res.repos, (repo, cb) => cloneRepo(res.accessToken, repo, cb), cb);
  }],
}, (err) => {
  if (err) {
    console.error(err);
    return process.exit(1);
  }

  process.exit(0);
});
