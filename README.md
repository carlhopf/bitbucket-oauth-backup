# bitbucket-backup

Clone all bitbucket repositories for a user to cwd.

```npm install -g git+https://carlhopf@bitbucket.org/carlhopf/bitbucket-oauth-backup.git```

## Command line usage

```bitbucket-oauth-backup <oauth-client-id> <oauth-client-secret> <bitbucket-username> <oauth-refresh-token>```
